import { Component, Input, Output, EventEmitter, OnChanges, OnInit, AfterViewInit, OnDestroy, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-img',
  templateUrl: './img.component.html',
  styleUrls: ['./img.component.scss']
})
export class ImgComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy{
imageDefault = 'https://www.shutterstock.com/shutterstock/photos/2086941550/display_1500/stock-vector-default-image-icon-vector-missing-picture-page-for-website-design-or-mobile-app-no-photo-2086941550.jpg'
img = '';
 @Input('img')
 set changeImg(newImg: string) {
    this.img = newImg;
    console.log('change just img => ', this.img)
 }

 @Input() alt = '';
 @Output() loaded = new EventEmitter<string>();
//  counter = 0;
//  counterFn: number | undefined

 constructor() {
  // before render
  // No async -- once time
  console.log('constructor', 'imgValue => ', this.img)
 }

ngOnInit(): void {
  // before render
  // async - fetch -- once time
  console.log('OnIniy', 'imgValue =>', this.img)
  // this.counterFn = window.setInterval(() => {
  //   this.counter += 1;
  //   console.log('run counter');
  // }, 1000)
}

 ngOnChanges(changes: SimpleChanges) {
  // before render and during
  // changes input --times
  console.log('ngOnChanges', 'imgValue =>', this.img)
  console.log('changes', changes)
 }

 ngAfterViewInit() {
  // after render
  // handle children
  console.log('ngAfterViewInit')
 }

 ngOnDestroy() {
   // delete
   console.log('ngOnDestroy')
   //window.clearInterval(this.counterFn)
 }


 imgError() {
    this.img = this.imageDefault
 }

 imgLoaded() {
  console.log('log hijo')
  this.loaded.emit(this.img)
 }


}
